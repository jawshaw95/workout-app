import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import { Button, IconButton } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { fetchWorkout } from "../../actions";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";

const useStyles = makeStyles({
  root: {
    minWidth: 275
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)"
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  }
});

export default function WorkoutCard(props) {
  const dispatch = useDispatch();

  const classes = useStyles();
  const { title, id } = props;
  return (
    <>
      <Card className={classes.root}>
        <CardContent>
          <Typography variant="h6" component="p">
            {title}
          </Typography>
        </CardContent>
        <CardActions>
          <Button size="small" component={Link} to={`workout/${id}`}>
            View Workout
          </Button>
          <IconButton component={Link} to={`workout/edit/${id}`}>
            <EditIcon />
          </IconButton>
          <IconButton>
            <DeleteIcon />
          </IconButton>
        </CardActions>
      </Card>
    </>
  );
}
