import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { signIn, signOut } from "../../actions/index";
import { Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  signInButton: {
    textTransform: "none",
    color: "white",
    backgroundColor: "purple"
  },
  signOutButton: {
    textTransform: "none",
    color: "red",
    backgroundColor: "white"
  }
}));

const fakeUser = {userId: '1234', name: 'Justin Shaw'};

export default function FakeAuth() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const isSignedIn = useSelector(state => state.auth.isSignedIn);

const onSignInClick = () => {
    dispatch(signIn(fakeUser))
  };

  const onSignOutClick = () => {
    dispatch(signOut())
  };

  const renderAuthButton = () => {
   if (isSignedIn) {
      return (
        <Button
          variant="outlined"
          className={classes.signOutButton}
          onClick={onSignOutClick}
        >
          Sign Out
        </Button>
      );
    } else {
      return (
        <Button
          variant="contained"
          className={classes.signInButton}
          onClick={onSignInClick}
        >
          Sign In
        </Button>
      );
    }
  };

  return <div>{renderAuthButton()}</div>;
}
