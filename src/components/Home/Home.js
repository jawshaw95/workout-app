import React from "react";
import FakeAuth from "../fakeAuth/FakeAuth";
//Todo  make image smaller
import HomePicture from "../../assets/duotone.png";
import { Typography} from "@material-ui/core";
import { makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  picture: {
    width: "100%",
    position: "absolute",
    height: "100vh"
  },
  titleContainer: {
    position: "absolute",
    top: "45%",
    left: "50%",
    transform: " translate(-50%, -50%)",
    color: "white",
    [theme.breakpoints.down('xs')] : {
        fontSize: "1.50em",
        top: '40%'
    }
  },
  loginContainer: {
    position: "absolute",
    paddingTop: "10px",
    top: "50%",
    left: "50%",
    transform: " translate(-50%, -50%)"
  }
}));

const Home = () => {
  const classes = useStyles();
  return (
    <>
      <img className={classes.picture} src={HomePicture} alt="login" />
          <Typography className={classes.titleContainer} variant="h3">
            Justin's Workout App
          </Typography>
          <div className={classes.loginContainer}>
            <FakeAuth />
          </div>
    </>
  );
};

export default Home;
