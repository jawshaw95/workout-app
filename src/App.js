import React from "react";
import Navbar from "./components/NavBar";
import { ThemeProvider } from "@material-ui/core/styles";
import { Router, Route, Switch } from "react-router-dom";
import theme from "./common/Theme";
import Home from "./components/Home/Home";
import history from "./history";
import { routes } from "./common/AppConstants";
import PrivateRoute from "./PrivateRoute";
import WorkoutContainer from "./components/workouts/WorkoutContainer";
import Workout from "./components/workouts/Workout";

function App() {
  const routeConfig = [
    { path: routes.workouts, component: WorkoutContainer },
    { path: routes.body, component: () => <div>Body</div> },
    { path: routes.workoutHistory, component: () => <div>Past Workouts</div> },
    { path: routes.workoutSettings, component: () => <div>SETTINGS</div> },
    { path: routes.profiles, component: () => <div>PROFILES</div> },
    { path: routes.workout, component: Workout }
  ];

  return (
    <ThemeProvider theme={theme}>
      <Router history={history}>
        <Route exact path="/" component={Home} />
        <PrivateRoute path="/u" component={Navbar} />
        <Switch>
          {routeConfig.map((route, index) => (
            <PrivateRoute
              key={`${index}`}
              exact
              path={route.path}
              component={route.component}
            />
          ))}
        </Switch>
      </Router>
    </ThemeProvider>
  );
}

export default App;
