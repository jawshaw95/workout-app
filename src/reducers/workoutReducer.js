import * as actions from "../actions/types";
import _ from "lodash";

export default (state = {}, action) => {
  switch (action.type) {
    case actions.FETCH_WORKOUTS:
      return { ...state, workoutList: action.payload };
    case actions.FETCH_WORKOUT:
      return { ...state, [action.payload.id]: action.payload };
    case actions.CREATE_WORKOUT:
      return { ...state, [action.payload.id]: action.payload };
    case actions.EDIT_WORKOUT:
      return { ...state, [action.payload.id]: action.payload };
    case actions.DELETE_WORKOUT:
      return _.omit(state, action.payload);
    default:
      return state;
  }
};
