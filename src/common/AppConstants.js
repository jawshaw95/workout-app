// Routes
export const routes = {
  login: "/",
  workouts: "/u/workouts",
  body: "/u/body",
  workoutHistory: "/u/workouts/history",
  workoutSettings: "/u/workouts/settings",
  workout: "/u/workout/:id",
  profiles: "/u/workouts/profiles"
};
