import React from "react";
import { Route, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";

export default function PrivateRoute({ component: Component, ...rest }) {
  const isAuth = useSelector(state => state.auth.isSignedIn);
  return (
    <Route
      {...rest}
      render={({ location }) => {
        return isAuth ? (
          <Component />
        ) : (
          <Redirect
            to={{
              pathname: "/",
              state: { from: location }
            }}
          />
        );
      }}
    />
  );
}
