import React, { useState, useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import AppBar from "@material-ui/core/AppBar";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import {
  Tabs,
  Tab,
  IconButton,
  Popper,
  Grow,
  Paper,
  ClickAwayListener,
  MenuItem,
  MenuList,
  SwipeableDrawer,
  ListItem,
  ListItemText,
  useMediaQuery
} from "@material-ui/core";
import { FitnessCenter, Person } from "@material-ui/icons";
import MenuIcon from "@material-ui/icons/Menu";
import { routes } from "../common/AppConstants";
import { useSelector } from "react-redux";
import FakeAuth from "./fakeAuth/FakeAuth";

const useStyles = makeStyles(theme => ({
  toolbarMargin: {
    ...theme.mixins.toolbar,
    marginBottom: ".5em"
  },
  tabContainer: {
    marginLeft: "auto"
  },
  tab: {
    ...theme.typography.tab,
    minWidth: 10,
    marginLeft: "25px"
  },
  drawerItemText: {
    ...theme.typography.tab,
    color: "#F6F7EB"
  },
  iconButton: {
    ...theme.iconButton
  },
  profileButton: {
    ...theme.iconButton,
    paddingLeft: "25px"
  },
  drawerContainer: {
    background: theme.palette.common.purple
  }
}));

function ElevationScroll(props) {
  const { children } = props;
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0
  });

  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0
  });
}

export default function NavBar() {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = useState(0);
  const [profileMenuOpen, setProfileMenuOpen] = useState(false);
  const [openDrawer, setOpenDrawer] = useState(false);
  const profile = useSelector(state => state.auth.userInfo);
  const anchorRef = useRef(null);
  const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);
  const matches = useMediaQuery(theme.breakpoints.down("md"));

  const tabConfig = [
    { label: "Workouts", to: routes.workouts, activeIndex: 0 },
    { label: "History", to: routes.workoutHistory, activeIndex: 1 },
    { label: "Body", to: routes.body, activeIndex: 2 },
    { label: "Settings", to: routes.workoutSettings, activeIndex: 3 }
  ];

  const routeMap = new Map();
  tabConfig.forEach(tab => {
    routeMap.set(tab.to, tab.activeIndex);
  });

  const handleToggle = () => {
    setProfileMenuOpen(prevOpen => !prevOpen);
  };

  const handleClose = event => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setProfileMenuOpen(false);
  };

  const prevOpen = React.useRef(profileMenuOpen);
  useEffect(() => {
    if (prevOpen.current === true && profileMenuOpen === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = profileMenuOpen;
  }, [profileMenuOpen]);

  const handleChange = (e, value) => {
    setValue(value);
  };

  useEffect(() => {
    if (value !== routeMap.get(window.location.pathname)) {
      setValue(routeMap.get(window.location.pathname));
    }
  }, [value]);

  function handleListKeyDown(event) {
    if (event.key === "Tab") {
      event.preventDefault();
      setProfileMenuOpen(false);
    }
  }

  const drawer = (
    <>
      <SwipeableDrawer
        disableBackdropTransition={!iOS}
        disableDiscovery={iOS}
        open={openDrawer}
        onClose={() => setOpenDrawer(false)}
        onOpen={() => setOpenDrawer(true)}
        classes={{ paper: classes.drawerContainer }}
      >
        {tabConfig.map((route, i) => (
          <ListItem
            key={`${route.label}-${i}`}
            onClick={() => {
              setOpenDrawer(false);
              setValue(route.activeIndex);
            }}
            divider
            button
            component={Link}
            to={route.to}
            selected={value === route.activeIndex}
            className={classes.drawerItemText}
          >
            <ListItemText>{route.label}</ListItemText>
          </ListItem>
        ))}
        <ListItem>
          <FakeAuth />
        </ListItem>
      </SwipeableDrawer>
      <IconButton
        className={classes.drawerIconContainer}
        onClick={() => setOpenDrawer(!openDrawer)}
        disableRipple
      >
        <MenuIcon className={classes.drawerIcon} />
      </IconButton>
    </>
  );

  const popupMenu = (
    <Popper
      open={profileMenuOpen}
      anchorEl={anchorRef.current}
      role={undefined}
      transition
      disablePortal
    >
      {({ TransitionProps, placement }) => (
        <Grow
          {...TransitionProps}
          style={{
            transformOrigin:
              placement === "bottom" ? "center top" : "center bottom"
          }}
        >
          <Paper>
            <ClickAwayListener onClickAway={handleClose}>
              <MenuList
                autoFocusItem={profileMenuOpen}
                id="menu-list-grow"
                onKeyDown={handleListKeyDown}
              >
                <MenuItem
                  onClick={handleClose}
                >{`${profile.name}`}</MenuItem>
                <MenuItem onClick={handleClose}>
                  <FakeAuth />
                </MenuItem>
              </MenuList>
            </ClickAwayListener>
          </Paper>
        </Grow>
      )}
    </Popper>
  );

  const tabs = (
    <>
      <Tabs
        value={value}
        onChange={handleChange}
        className={classes.tabContainer}
        variant="fullWidth"
      >
        {tabConfig.map((tab, index) => (
          <Tab
            className={classes.tab}
            component={Link}
            key={`${tab.label}-${index}`}
            label={tab.label}
            to={tab.to}
          />
        ))}
      </Tabs>
      <IconButton
        ref={anchorRef}
        aria-controls={profileMenuOpen ? "menu-list-grow" : undefined}
        aria-haspopup="true"
        className={classes.profileButton}
        onClick={() => {
          handleToggle();
        }}
      >
        <Person></Person>
      </IconButton>
      {popupMenu}
    </>
  );

  //TODO do i want this button
  const currentWorkoutButton = (
    <IconButton
      className={classes.iconButton}
      onClick={() => setValue("")}
      component={Link}
      to="/"
    >
      <FitnessCenter />
    </IconButton>
  );

  return (
    <>
      <ElevationScroll>
        <AppBar>
          <Toolbar disableGutters className={classes.toolbar}>
            {matches ? drawer : tabs}
          </Toolbar>
        </AppBar>
      </ElevationScroll>
      <div className={classes.toolbarMargin} />
    </>
  );
}
