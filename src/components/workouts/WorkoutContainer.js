import React, { useEffect } from "react";
import { fetchWorkouts } from "../../actions/index";
import { useDispatch, useSelector } from "react-redux";
import WorkoutCard from "./WorkoutCard";
import { Grid } from "@material-ui/core";

export default function WorkoutContainer() {
  const dispatch = useDispatch();
  const workouts = useSelector(state => state.workouts.workoutList);

  useEffect(() => {
    async function dispatchFetchWorkouts() {
      await dispatch(fetchWorkouts());
    }

    dispatchFetchWorkouts();
  }, []);

  return (
    <>
      <Grid
        container
        spacing={2}
        alignItems="center"
        justify="center"
        direction="row"
      >
        {workouts &&
          workouts.map((workout, index) => (
            <Grid key={`${workout.title}-${index}`} item>
              <WorkoutCard title={workout.title} id={workout.id} />
            </Grid>
          ))}
      </Grid>
    </>
  );
}
