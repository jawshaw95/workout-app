import * as actions from "./types";
import workouts from "../apis/workouts";
import history from "../history";
import {routes} from '../common/AppConstants'

export const signIn = userInfo => dispatch => {
  dispatch({
    type: actions.SIGN_IN,
    payload: userInfo
  });
  history.push(routes.workouts);
};

export const signOut = () => dispatch => {
  dispatch({
    type: actions.SIGN_OUT
  });
  history.push(routes.login);
};

export const createWorkout = formValues => async (dispatch, getState) => {
  const { userId } = getState().auth;
  const response = await workouts.post("/workouts", { ...formValues, userId });

  dispatch({ type: actions.CREATE_WORKOUT, payload: response.data });
  history.push(routes.workouts);
};

export const fetchWorkouts = () => async dispatch => {
  const response = await workouts.get('workouts');

  dispatch({
    type: actions.FETCH_WORKOUTS,
    payload: response.data
  });
};

export const fetchWorkout = id => async dispatch => {
  const response = await workouts.get(`/workouts/${id}`);

  dispatch({
    type: actions.FETCH_WORKOUT,
    payload: response.data
  });
};

export const editWorkout = (id, formValues) => async dispatch => {
  const response = await workouts.patch(`/workouts/${id}`, formValues);

  dispatch({
    type: actions.EDIT_WORKOUT,
    payload: response.data
  });
  history.push(routes.workouts);
};

export const deleteWorkout = id => async dispatch => {
  await workouts.delete(`/workouts/${id}`);

  dispatch({
    type: actions.DELETE_WORKOUT,
    payload: id
  });
  history.push(routes.workouts);
};
