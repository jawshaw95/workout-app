import { createMuiTheme } from "@material-ui/core/styles";

const primaryPurple = "#42113C";
const primaryGreen = "#688E26";

export default createMuiTheme({
  palette: {
    common: {
      purple: `${primaryPurple}`,
      green: `${primaryGreen}`
    },
    primary: {
      main: `${primaryPurple}`
    },
    secondary: {
      main: `${primaryGreen}`
    }
  },
  typography: {
    tab: {
      fontFamily: "Raleway",
      textTransform: "none",
      fontWeight: 700,
      fontSize: "1rem"
    },
    estimate: {
      fontFamily: "Pacifico",
      fontSize: "1rm",
      textTransform: "none",
      color: "#F6F7EB"
    }
  },
  iconButton: {
    "&:hover": {
      backgroundColor: "transparent"
    },
    color: "#F6F7EB"
  }
});
