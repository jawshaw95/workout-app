import * as actions from "../actions/types";

const INITIAL_STATE = {
  userInfo: {},
  isSignedIn: null
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case actions.SIGN_IN:
      return {
        ...state,
        isSignedIn: true,
        userInfo: action.payload
      };
    case actions.SIGN_OUT:
      return { ...state, isSignedIn: false, userInfo: null };
    default:
      return state;
  }
};
