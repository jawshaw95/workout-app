import React, { useEffect } from "react";
import { fetchWorkout } from "../../actions";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";

export default function Workout(props) {
  const { id } = useParams();
  const dispatch = useDispatch();
  const workout = useSelector(state => state.workouts[id]);

  useEffect(() => {
    async function dispatchFetchWorkout() {
      await dispatch(fetchWorkout(id));
    }

    dispatchFetchWorkout();
  }, []);

  console.log("poops", props);
  return <>{workout && workout.title}</>;
}
